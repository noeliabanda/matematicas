package Audio;


import Audio.*;


import java.awt.*;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.io.*;
import javax.sound.sampled.*;
import javax.swing.*;
import GUI.*;
/**
 *
 * @author YeeyaBanda
 */
public class Principal extends javax.swing.JFrame {
    
private double[] datosVoz = null; //vector de amplitud de la señal
private double fex; //factor de escalamiento en las abscisas
private double fey; //factor de escalamiento en las coorenadas
private int FrecuenciaDeSampleo; //frecuencia de muestras de la señal
private int finterv; //factor de intervalo de muestreo de la señal
private String Informe; //informacion de las propiedades de la señal
private String NameAudio; //nombre del archivo de audio
private String rutaAudio; //ruta del archivo de audio
private GuiSeñal panelSeñal; //JPanel para la graficación
private GuiSeñal Grafica;


JFileChooser fc = new JFileChooser();



   // GuiSeñal fc = new GuiSeñal();
    public Principal() {
        initComponents();
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menuAbrir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        menuAbrir.setText("Abrir");
        menuAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAbrirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(166, 166, 166)
                .addComponent(menuAbrir)
                .addContainerGap(179, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(131, 131, 131)
                .addComponent(menuAbrir)
                .addContainerGap(146, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void menuAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAbrirActionPerformed
        // TODO add your handling code here:
        if (evt.getSource() == menuAbrir){             
            int valorRetorno = fc.showOpenDialog(null);
            if (valorRetorno == JFileChooser.APPROVE_OPTION) {                
                File archivoEntrada = fc.getSelectedFile();         
                rutaAudio = archivoEntrada.getPath();
                System.out.println("Ruta del audio = "+rutaAudio);            
                int totalFramesLeidos = 0;
                int totalBytesLeidos = 0;
                try {
                    AudioInputStream flujoEntradaAudio = AudioSystem.getAudioInputStream(archivoEntrada);
                    int bytesPorFrame = flujoEntradaAudio.getFormat().getFrameSize();                        
                    int numBytes = 1024 * bytesPorFrame; 
                    byte[] audioBytes = new byte[numBytes];
                    
                    int longitudArchivoBytes=(int)flujoEntradaAudio.getFormat().getFrameSize()*(int)flujoEntradaAudio.getFrameLength();
                    Datos datos = new Datos(longitudArchivoBytes,flujoEntradaAudio.getFormat().isBigEndian());
                    byte[] datosTemporal=new byte[longitudArchivoBytes];     
                    int pos=0;         
                    try {
                        int numeroBytesLeidos = 0;
                        int numeroFramesLeidos = 0;                    
                        while((numeroBytesLeidos=flujoEntradaAudio.read(audioBytes))!=-1) {                      
                            numeroFramesLeidos = numeroBytesLeidos/bytesPorFrame;
                            totalFramesLeidos += numeroFramesLeidos;                  
                            System.arraycopy(audioBytes, 0, datosTemporal,pos, numeroBytesLeidos);
                            pos=pos+numeroBytesLeidos;                      
                        }
                        datos.llenarByte(datosTemporal);
                        datosVoz=new double[longitudArchivoBytes/bytesPorFrame];
                        datosVoz=datos.convertirByteADouble();
                        graficarSeñal();
                        Informe = "";                      
                        Informe+="Abriendo:   "+archivoEntrada.getName()+".\n";
                        Informe+="Ruta: "+rutaAudio+"\n\n";       
                        Informe+="   NroBytes    Frames    BigEndian \n";

                        Informe+="   "+longitudArchivoBytes+"    "+
                               flujoEntradaAudio.getFrameLength()+"    "+
                                flujoEntradaAudio.getFormat().isBigEndian()+"    \n";
                        
                        Informe+="\n Formato: "+flujoEntradaAudio.getFormat().toString();
                        Informe+="\n\n Tamaño del arreglo: "+datosVoz.length+"\n"; 
                        
                        FrecuenciaDeSampleo = (int)flujoEntradaAudio.getFormat().getSampleRate();
                        Informe+=" Frecuencia de sampleo: "+FrecuenciaDeSampleo+"m/s\n";
                        Reproducir audioR = new Reproducir(rutaAudio);                        
                        audioR.playAudio();                                                            
                    } 
                    catch (Exception ex) { 
                        Informe = "No se pudo leer \n";                              
                    }                        
                } 
                catch (Exception e) {          
                    Informe = "Tipo incompatible\n";    
                }                                                                        
            } else {                
                Informe = "Acción de abrir cancelada por el usuario\n";
            }                                                 
        }
        minimizar();
    }//GEN-LAST:event_menuAbrirActionPerformed

    public void minimizar(){
        this.hide();
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }
    
    private void graficarSeñal(){ 
        panelSeñal.setDatosVoz(datosVoz);
        panelSeñal.setAccionGraficar(true);
        panelSeñal.setGraficarOnda(!panelSeñal.getGraficarOnda());
        panelSeñal.setIntervalo((finterv*datosVoz.length)/Grafica.getWidth());
        panelSeñal.setFactorEX(fex);
        panelSeñal.setFactorEY(fey);
        panelSeñal.setTY(0);
        panelSeñal.repaint();    
    } 

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton menuAbrir;
    // End of variables declaration//GEN-END:variables
}
