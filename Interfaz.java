
package fdp;


import java.awt.Button;
import java.awt.FlowLayout;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author YeeyaBanda
 */
public class Interfaz extends javax.swing.JFrame {
    
    Map datos=new HashMap();
    
    
    ArrayList<Double> num1=new ArrayList<Double>();
    ArrayList<Double> num2=new ArrayList<Double>();
    
    public Interfaz() {
        initComponents();
               
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cajaTexto = new javax.swing.JTextField(10);
        jButton1 = new javax.swing.JButton();
        buttonAction2 = new Button("Función Densidad");
        buttonAction1 = new Button("Función Distribución");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new FlowLayout());
        

        jLabel1.setText("Ruta:");
        getContentPane().add(jLabel1);

        cajaTexto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cajaTextoActionPerformed(evt);
            }
        });
        getContentPane().add(cajaTexto);

        jButton1.setText("Examinar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);

        //buttonAction2.setText("Funcion Densidad");
        buttonAction2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAction2ActionPerformed(evt);
            }
        });
        getContentPane().add(buttonAction2);

       // buttonAction1.setText("Funcion Distribucion");
        buttonAction1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAction1ActionPerformed(evt);
            }
        });
        getContentPane().add(buttonAction1);

        pack();
    }

    private void cajaTextoActionPerformed(java.awt.event.ActionEvent evt) {
        
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
       JFileChooser filechooser=new JFileChooser();
       int opcion=filechooser.showOpenDialog(this);
       if(opcion==JFileChooser.APPROVE_OPTION){
           cajaTexto.setText(filechooser.getSelectedFile().getPath());
       }
       
        String texto=null;
        StringTokenizer st=null;
        try{
            FileReader fr=new FileReader(cajaTexto.getText());
            BufferedReader br=new BufferedReader(fr);
            while((texto=br.readLine())!=null){
                st=new StringTokenizer(texto,",");

                while(st.hasMoreTokens()){
                    
                    num1.add(Double.parseDouble(st.nextToken()));
                    num2.add(Double.parseDouble(st.nextToken()));
                }
            }
        }catch(FileNotFoundException ex){
            System.out.println("No existe el fichero");
        }catch(Exception ex){
            System.out.println("Error en E/S");
            ex.printStackTrace();
        }
       
    }

    private void buttonAction2ActionPerformed(java.awt.event.ActionEvent evt) {
       
        DefaultCategoryDataset Datos=new DefaultCategoryDataset();
        
        for(int i=0;i<num1.size();i++){
          
           Datos.addValue(num2.get(i),"p(x)",num1.get(i));
           
        }
        
        JFreeChart Grafica = ChartFactory.createBarChart("Función de Densidad de Probabilidad","x","F(x)",Datos,PlotOrientation.VERTICAL,true,true,false);
        
        ChartPanel Panel=new ChartPanel(Grafica);
        JFrame Ventana=new JFrame("Titulo");
        Ventana.getContentPane().add(Panel);
        Ventana.pack();
        Ventana.setVisible(true);
        Ventana.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
    }

    private void buttonAction1ActionPerformed(java.awt.event.ActionEvent evt) {
        DefaultCategoryDataset Datos=new DefaultCategoryDataset();
        double aux=0;
        for(int i=0;i<num1.size();i++){
          
           Datos.addValue(aux+=num2.get(i),"p(x)",num1.get(i));
           
        }
        
        JFreeChart Grafica = ChartFactory.createBarChart("Funcion de Distribución acumulada","x","F(x)",Datos,PlotOrientation.VERTICAL,true,true,false);
        
        ChartPanel Panel=new ChartPanel(Grafica);
        JFrame Ventana=new JFrame("Titulo");
        Ventana.getContentPane().add(Panel);
        Ventana.pack();
        Ventana.setVisible(true);
        Ventana.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
    
    public static void main(String args[]) {
            try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InterfazGrafica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InterfazGrafica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InterfazGrafica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InterfazGrafica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Interfaz().setVisible(true);
            }
        });
    } 

    private Button buttonAction1;
    private Button buttonAction2;
    private javax.swing.JTextField cajaTexto;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    
}